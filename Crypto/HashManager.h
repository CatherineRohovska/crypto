//
//  HashManager.h
//  Crypto
//
//  Created by User on 10/15/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Security/Security.h>
#include <CommonCrypto/CommonDigest.h>
#import <CoreFoundation/CoreFoundation.h>
@interface HashManager : NSObject
{
    
}
/**
 * Gets access to manager. Generates pair of keys inside.
 */
+ (id)sharedManager;
/**
 * Signs file. Saves hash in local storage.
 * @param name Name of file
 */
-(void) signSHA256withRSA: (NSString*) name;
/**
 * Verify file using local storage
 * @param name Name of file
 * @return YES or NO depends on equiality of signatures
 */
- (BOOL) verifySHA256withRSA: (NSString*) name;
@end
