//
//  HashManager.m
//  Crypto
//
//  Created by User on 10/15/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "HashManager.h"
@interface HashManager()
{   NSString* publicKeyIdentifier;
    NSString* privateKeyIdentifier;
    OSStatus status;

}

@end
@implementation HashManager
+ (id)sharedManager {
    static HashManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}
- (id)init {
    if (self = [super init]) {
        SecKeyRef publicKey;
        SecKeyRef privateKey;

        //1 creates key identifier from application ID
        privateKeyIdentifier = [[[NSBundle mainBundle] bundleIdentifier] stringByAppendingString:@"private\0"];
        publicKeyIdentifier = [[[NSBundle mainBundle] bundleIdentifier] stringByAppendingString:@"public\0"];
        status = noErr;
        NSMutableDictionary *privateKeyAttr = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *publicKeyAttr = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *keyPairAttr = [[NSMutableDictionary alloc] init];
        // 2 Allocates dictionaries to be used for attributes in the SecKeyGeneratePair function.
        
        NSData * publicTag = [NSData dataWithBytes:[publicKeyIdentifier UTF8String]
                                            length:[publicKeyIdentifier length]];
        NSData * privateTag = [NSData dataWithBytes:[privateKeyIdentifier UTF8String]
                                             length:[privateKeyIdentifier length]];
        // 3 Creates NSData objects that contain the identifier strings in 1
        
        publicKey = NULL;
        privateKey = NULL;                                // 4 Allocates SecKeyRef objects for the public and private keys.
        
        [keyPairAttr setObject:(__bridge id)kSecAttrKeyTypeRSA
                        forKey:(__bridge id)kSecAttrKeyType]; // 5 Sets the key-type attribute for the key pair to RSA.
        [keyPairAttr setObject:[NSNumber numberWithInt:1024]
                        forKey:(__bridge id)kSecAttrKeySizeInBits]; // 6 Sets the key-size attribute for the key pair to 1024 bits.
        
        [privateKeyAttr setObject:[NSNumber numberWithBool:YES]
                           forKey:(__bridge id)kSecAttrIsPermanent]; // 7 Sets an attribute specifying that the private key is to be stored permanently (that is, put into the keychain).
        [privateKeyAttr setObject:privateTag
                           forKey:(__bridge id)kSecAttrApplicationTag]; // 8 Adds the identifier string defined in steps 1 and 3 to the dictionary for the private key.
        
        [publicKeyAttr setObject:[NSNumber numberWithBool:YES]
                          forKey:(__bridge id)kSecAttrIsPermanent]; // 9 Sets an attribute specifying that the public key is to be stored permanently (that is, put into the keychain).
        [publicKeyAttr setObject:publicTag
                          forKey:(__bridge id)kSecAttrApplicationTag]; // 10 Adds the identifier string defined in steps 1 and 3 to the dictionary for the public key.
        
        [keyPairAttr setObject:privateKeyAttr
                        forKey:(__bridge id)kSecPrivateKeyAttrs]; // 11 Adds the dictionary of private key attributes to the key-pair dictionary.
        [keyPairAttr setObject:publicKeyAttr
                        forKey:(__bridge id)kSecPublicKeyAttrs]; // 12 Adds the dictionary of public key attributes to the key-pair dictionary.
        
        status = SecKeyGeneratePair((__bridge CFDictionaryRef)keyPairAttr,
                                    &publicKey, &privateKey); // 13 Generates the key pair.
        
    }
    return self;
}
//sign data
-(void) signSHA256withRSA:  (NSString*) name{
    //
    NSData* plainData; //data to sign
    NSError *error = nil;
    //load data from file
    NSString* fileData = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:name ofType:nil] encoding:NSUTF8StringEncoding error:&error];
    plainData = [NSData dataWithBytes:[fileData UTF8String] length:[fileData length]];
    SecKeyRef privateKey;
    privateKey = NULL;
    //create tag for finding private key
    NSData * privateTag = [NSData dataWithBytes:[privateKeyIdentifier UTF8String]
                                         length:[privateKeyIdentifier length]];
    
    NSMutableDictionary *queryPrivateKey = [[NSMutableDictionary alloc] init];
    
    // Set the private key query dictionary.
    [queryPrivateKey setObject:(__bridge id)kSecClassKey forKey:(__bridge id)kSecClass];
    [queryPrivateKey setObject:privateTag forKey:(__bridge id)kSecAttrApplicationTag];
    [queryPrivateKey setObject:(__bridge id)kSecAttrKeyTypeRSA forKey:(__bridge id)kSecAttrKeyType];
    [queryPrivateKey setObject:[NSNumber numberWithBool:YES] forKey:(__bridge id)kSecReturnRef];
    // get key from dictionary
    
    status = SecItemCopyMatching((__bridge CFDictionaryRef)queryPrivateKey, (CFTypeRef *)&privateKey);
    //allocates memory for signing
    size_t signedHashBytesSize = SecKeyGetBlockSize(privateKey);
    uint8_t* signedHashBytes = malloc(signedHashBytesSize);
    memset(signedHashBytes, 0x0, signedHashBytesSize);
    
    size_t hashBytesSize = CC_SHA256_DIGEST_LENGTH;
    uint8_t* hashBytes = malloc(hashBytesSize);
    if (!CC_SHA256([plainData bytes], (CC_LONG)[plainData length], hashBytes)) {
        return;
    }
    
    status = SecKeyRawSign(privateKey,
                           kSecPaddingPKCS1SHA256,
                           hashBytes,
                           hashBytesSize,
                           signedHashBytes,
                           &signedHashBytesSize);
    
    NSData* signedHash = [NSData dataWithBytes:signedHashBytes
                                        length:(NSUInteger)signedHashBytesSize];
    //deallocate memory that we don't need
    if (hashBytes)
        free(hashBytes);
    if (signedHashBytes)
        free(signedHashBytes);
    //add data by pair of attributes - name of application and name of file itself
    NSDictionary *query = @{
                            (id)kSecClass : (id)kSecClassGenericPassword,
                            (id)kSecAttrService : [[NSBundle mainBundle] bundleIdentifier],
                            (id)kSecAttrAccount : name,
                            (id)kSecValueData : signedHash
                            };
    //query for cheking existence
    NSDictionary *queryExist = @{
                            (id)kSecClass : (id)kSecClassGenericPassword,
                            (id)kSecAttrService : [[NSBundle mainBundle] bundleIdentifier],
                            (id)kSecAttrAccount : name,
                            (id)kSecReturnData : @YES
                            };
    
    if (SecItemCopyMatching((CFDictionaryRef)queryExist, NULL)!=0){
        status = SecItemAdd((CFDictionaryRef)query, NULL);}
    else{
        status = SecItemDelete((CFDictionaryRef)queryExist);
        status = SecItemAdd((CFDictionaryRef)query, NULL);
        }
   

}
//verify data
- (BOOL) verifySHA256withRSA: (NSString*) name{
    //creating data by file name
    NSData* plainData;
    NSError *error = nil;
    //load data from file
    NSString* fileData = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:name ofType:nil] encoding:NSUTF8StringEncoding error:&error];
    plainData = [NSData dataWithBytes:[fileData UTF8String] length:[fileData length]];
    // finding signature file by name
    CFDataRef  signature= NULL;
    NSDictionary *query = @{
                                 (id)kSecClass : (id)kSecClassGenericPassword,
                                 (id)kSecAttrService : [[NSBundle mainBundle] bundleIdentifier],
                                 (id)kSecAttrAccount : name,
                                 (id)kSecReturnData : @YES
                                 };
    status = SecItemCopyMatching((CFDictionaryRef)query, (CFTypeRef*)&signature);

    //

    SecKeyRef publicKey;
    //creates tag for finding public key
    NSData * publicTag = [NSData dataWithBytes:[publicKeyIdentifier UTF8String]
                                        length:[publicKeyIdentifier length]];
    // set query to find public key
    NSMutableDictionary *queryPublicKey = [[NSMutableDictionary alloc] init];
    
    [queryPublicKey setObject:(__bridge id)kSecClassKey forKey:(__bridge id)kSecClass];
    [queryPublicKey setObject:publicTag forKey:(__bridge id)kSecAttrApplicationTag];
    [queryPublicKey setObject:(__bridge id)kSecAttrKeyTypeRSA forKey:(__bridge id)kSecAttrKeyType];
    [queryPublicKey setObject:[NSNumber numberWithBool:YES] forKey:(__bridge id)kSecReturnRef];
 
    //perform query
    status = SecItemCopyMatching ((__bridge CFDictionaryRef)queryPublicKey, (CFTypeRef *)&publicKey);
    // allocates memory for previous hash
    size_t signedHashBytesSize = SecKeyGetBlockSize(publicKey);
    const void* signedHashBytes = CFDataGetBytePtr(signature);
    //allocates memory for current hash
    size_t hashBytesSize = CC_SHA256_DIGEST_LENGTH;
    uint8_t* hashBytes = malloc(hashBytesSize);
    if (!CC_SHA256([plainData bytes], (CC_LONG)[plainData length], hashBytes)) {
        return nil;
    }
    //compare
    status = SecKeyRawVerify(publicKey,
                                      kSecPaddingPKCS1SHA256,
                                      hashBytes,
                                      hashBytesSize,
                                      signedHashBytes,
                                      signedHashBytesSize);
    
    return status == errSecSuccess;

}
@end
