//
//  ViewController.m
//  Crypto
//
//  Created by User on 10/13/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "ViewController.h"
#import <Security/Security.h>
#include <CommonCrypto/CommonDigest.h>
#import <CoreFoundation/CoreFoundation.h>
#import "HashManager.h"
@interface ViewController (){
  
    //enc
    OSStatus status;
    SecKeyRef publicKey;
    SecKeyRef privateKey;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    HashManager* hm = [HashManager sharedManager];
   // [self generateKeyPair];
   //[self decryptWithPrivateKey: [self encryptWithPublicKey]];
 
   
    [hm signSHA256withRSA:@"test.txt"];
    [hm verifySHA256withRSA:@"test.txt"];
   // [hm VerifySHA256withRSA:data sign:[hm SignSHA256withRSA:data]];
//    [self PKCSVerifyBytesSHA256withRSA:data sign:[self PKCSSignBytesSHA256withRSA:data key: privateKey] key:publicKey] ;
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/// 1
static const UInt8 publicKeyIdentifier[] = "com.apple.sample.publickey\0";
static const UInt8 privateKeyIdentifier[] = "com.apple.sample.privatekey\0";

- (void)generateKeyPair
{
    
    status = noErr;
    NSMutableDictionary *privateKeyAttr = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *publicKeyAttr = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *keyPairAttr = [[NSMutableDictionary alloc] init];
    // 2 Allocates dictionaries to be used for attributes in the SecKeyGeneratePair function.
    
    NSData * publicTag = [NSData dataWithBytes:publicKeyIdentifier
                                        length:strlen((const char *)publicKeyIdentifier)];
    NSData * privateTag = [NSData dataWithBytes:privateKeyIdentifier
                                         length:strlen((const char *)privateKeyIdentifier)];
    // 3 Creates NSData objects that contain the identifier strings in 1
    
    publicKey = NULL;
    privateKey = NULL;                                // 4 Allocates SecKeyRef objects for the public and private keys.
    
    [keyPairAttr setObject:(__bridge id)kSecAttrKeyTypeRSA
                    forKey:(__bridge id)kSecAttrKeyType]; // 5 Sets the key-type attribute for the key pair to RSA.
    [keyPairAttr setObject:[NSNumber numberWithInt:1024]
                    forKey:(__bridge id)kSecAttrKeySizeInBits]; // 6 Sets the key-size attribute for the key pair to 1024 bits.
    
    [privateKeyAttr setObject:[NSNumber numberWithBool:YES]
                       forKey:(__bridge id)kSecAttrIsPermanent]; // 7 Sets an attribute specifying that the private key is to be stored permanently (that is, put into the keychain).
    [privateKeyAttr setObject:privateTag
                       forKey:(__bridge id)kSecAttrApplicationTag]; // 8 Adds the identifier string defined in steps 1 and 3 to the dictionary for the private key.
    
    [publicKeyAttr setObject:[NSNumber numberWithBool:YES]
                      forKey:(__bridge id)kSecAttrIsPermanent]; // 9 Sets an attribute specifying that the public key is to be stored permanently (that is, put into the keychain).
    [publicKeyAttr setObject:publicTag
                      forKey:(__bridge id)kSecAttrApplicationTag]; // 10 Adds the identifier string defined in steps 1 and 3 to the dictionary for the public key.
    
    [keyPairAttr setObject:privateKeyAttr
                    forKey:(__bridge id)kSecPrivateKeyAttrs]; // 11 Adds the dictionary of private key attributes to the key-pair dictionary.
    [keyPairAttr setObject:publicKeyAttr
                    forKey:(__bridge id)kSecPublicKeyAttrs]; // 12 Adds the dictionary of public key attributes to the key-pair dictionary.
    
    status = SecKeyGeneratePair((__bridge CFDictionaryRef)keyPairAttr,
                                &publicKey, &privateKey); // 13 Generates the key pair.
    //    error handling...
    
    
//   if(publicKey) CFRelease(publicKey);
//   if(privateKey) CFRelease(privateKey);                       // 14 Releases memory that is no longer needed.
}
//sign
-(NSData*) PKCSSignBytesSHA256withRSA: (NSData*) plainData  key:(SecKeyRef) privateKey
{
    //
//    const uint8_t dataToEncrypt[] = "the quick brown fox jumps "
//    "over the lazy dog\0";
//    size_t dataLength = sizeof(dataToEncrypt)/sizeof(dataToEncrypt[0]);
//
    size_t signedHashBytesSize = SecKeyGetBlockSize(privateKey);
    uint8_t* signedHashBytes = malloc(signedHashBytesSize);
    memset(signedHashBytes, 0x0, signedHashBytesSize);
    
    size_t hashBytesSize = CC_SHA256_DIGEST_LENGTH;
    uint8_t* hashBytes = malloc(hashBytesSize);
    if (!CC_SHA256([plainData bytes], (CC_LONG)[plainData length], hashBytes)) {
        return nil;
    }
    
    status = SecKeyRawSign(privateKey,
                  kSecPaddingPKCS1SHA256,
                  hashBytes,
                  hashBytesSize,
                  signedHashBytes,
                  &signedHashBytesSize);
    
    NSData* signedHash = [NSData dataWithBytes:signedHashBytes
                                        length:(NSUInteger)signedHashBytesSize];
    
    if (hashBytes)
        free(hashBytes);
    if (signedHashBytes)
        free(signedHashBytes);
    
    return signedHash;
}
//verify
- (BOOL) PKCSVerifyBytesSHA256withRSA: (NSData*) plainData  sign: (NSData*) signature key:(SecKeyRef) publicKey
{
    size_t signedHashBytesSize = SecKeyGetBlockSize(publicKey);
    const void* signedHashBytes = [signature bytes];
    
    size_t hashBytesSize = CC_SHA256_DIGEST_LENGTH;
    uint8_t* hashBytes = malloc(hashBytesSize);
    if (!CC_SHA256([plainData bytes], (CC_LONG)[plainData length], hashBytes)) {
        return nil;
    }
    
    OSStatus status = SecKeyRawVerify(publicKey,
                                      kSecPaddingPKCS1SHA256,
                                      hashBytes,
                                      hashBytesSize,
                                      signedHashBytes,
                                      signedHashBytesSize);
    
    return status == errSecSuccess;
}
//encryption
- (NSData *)encryptWithPublicKey
{
    size_t cipherBufferSize;
    uint8_t *cipherBuffer;
    // preapring data to encrypt;
    const uint8_t dataToEncrypt[] = "the quick brown fox jumps "
    "over the lazy dog\0";
    size_t dataLength = sizeof(dataToEncrypt)/sizeof(dataToEncrypt[0]);
    //
   // SecKeyRef publicKey = NULL;                                 // 3
    
    NSData * publicTag = [NSData dataWithBytes:publicKeyIdentifier
                                        length:strlen((const char *)publicKeyIdentifier)]; // 4
    
    NSMutableDictionary *queryPublicKey =
    [[NSMutableDictionary alloc] init]; // 5
    
    [queryPublicKey setObject:(__bridge id)kSecClassKey forKey:(__bridge id)kSecClass];
    [queryPublicKey setObject:publicTag forKey:(__bridge id)kSecAttrApplicationTag];
    [queryPublicKey setObject:(__bridge id)kSecAttrKeyTypeRSA forKey:(__bridge id)kSecAttrKeyType];
    [queryPublicKey setObject:[NSNumber numberWithBool:YES] forKey:(__bridge id)kSecReturnRef];
    // 6
    
    status = SecItemCopyMatching
    ((__bridge CFDictionaryRef)queryPublicKey, (CFTypeRef *)&publicKey);
    //  Allocate a buffer
    
    cipherBufferSize = SecKeyGetBlockSize(publicKey);
    cipherBuffer = malloc(cipherBufferSize);
    
    //  Error handling
    
    if (cipherBufferSize < sizeof(dataToEncrypt)) {
        // Ordinarily, you would split the data up into blocks
        // equal to cipherBufferSize, with the last block being
        // shorter. For simplicity, this example assumes that
        // the data is short enough to fit.
        printf("Could not decrypt.  Packet too large.\n");
        return NULL;
    }
    
    // Encrypt using the public.
    status = SecKeyEncrypt(    publicKey,
                           kSecPaddingPKCS1,
                           dataToEncrypt,
                           (size_t) dataLength,
                           cipherBuffer,
                           &cipherBufferSize
                           );// 8

    //  Error handling
    //  Store or transmit the encrypted text
    
   //if (publicKey) CFRelease(publicKey);
    
    NSData *encryptedData = [NSData dataWithBytes:cipherBuffer length:cipherBufferSize];
    
    free(cipherBuffer);
    
    return encryptedData;
}
//decryption
- (void)decryptWithPrivateKey: (NSData *)dataToDecrypt
{
    size_t cipherBufferSize = [dataToDecrypt length];
    uint8_t *cipherBuffer = (uint8_t *)[dataToDecrypt bytes];
    
    size_t plainBufferSize;
    uint8_t *plainBuffer;
    //
     privateKey = NULL;
    
    NSData * privateTag = [NSData dataWithBytes:privateKeyIdentifier
                                         length:strlen((const char *)privateKeyIdentifier)];
    
    NSMutableDictionary *queryPrivateKey = [[NSMutableDictionary alloc] init];
    
    // Set the private key query dictionary.
    [queryPrivateKey setObject:(__bridge id)kSecClassKey forKey:(__bridge id)kSecClass];
    [queryPrivateKey setObject:privateTag forKey:(__bridge id)kSecAttrApplicationTag];
    [queryPrivateKey setObject:(__bridge id)kSecAttrKeyTypeRSA forKey:(__bridge id)kSecAttrKeyType];
    [queryPrivateKey setObject:[NSNumber numberWithBool:YES] forKey:(__bridge id)kSecReturnRef];
    // 1
    
    status = SecItemCopyMatching
    ((__bridge CFDictionaryRef)queryPrivateKey, (CFTypeRef *)&privateKey);
    //  Allocate the buffer
    plainBufferSize = SecKeyGetBlockSize(privateKey);
    plainBuffer = malloc(plainBufferSize);
    
    if (plainBufferSize < cipherBufferSize) {
        // Ordinarily, you would split the data up into blocks
        // equal to plainBufferSize, with the last block being
        // shorter. For simplicity, this example assumes that
        // the data is short enough to fit.
        printf("Could not decrypt.  Packet too large.\n");
        return;
    }
    
    //  Error handling
    
    status = SecKeyDecrypt( privateKey,
                           kSecPaddingPKCS1,
                           cipherBuffer,
                           (size_t)cipherBufferSize,
                           plainBuffer,
                           &plainBufferSize
                           );
    
    //  Error handling
    //  Store or display the decrypted text
    NSData *decryptedData = [NSData dataWithBytes:plainBuffer length:plainBufferSize];
    NSString* str = [[NSString alloc] initWithData:decryptedData encoding:NSUTF8StringEncoding];
//    uint8_t* str =  malloc(plainBufferSize);
//    memcpy(str, [decryptedData bytes], plainBufferSize);//(uint8_t *)[decryptedData bytes];
    ///
//    NSUInteger len = [decryptedData length];
//    Byte *byteData = (Byte*)malloc(len);
//    memcpy(byteData, [decryptedData bytes], len);
    uint8_t* array = (uint8_t *)[decryptedData bytes];
    free(cipherBuffer);
   if(privateKey) CFRelease(privateKey);
}
@end
